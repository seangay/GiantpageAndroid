import React, { Component } from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity, Text, ScrollView, ToastAndroid } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';

export default class SignUp extends Component {
 
    constructor(props) {
    	super(props);
    	this.state = {
    	    first_name: '',
            last_name: '',
            phone_number: '',
    	    email: '',
    	    password: '',
    	    c_password: '',
    	};
    }
  
    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="Sign Up" />
                    <Content>
                        <ScrollView>
                            <View style={styles.container}>
                                <View style={styles.content}>
                                    <Text style={styles.text}>Create a New Account !!!</Text>
                                    <Text style={styles.colorText}>It’s free and always will be.</Text>
                                </View>
                                <TextInput        
                                    ///Hint text input            
                                    placeholder="Enter first name"            
                                    onChangeText={(text) => this.setState({first_name: text})}            
                                    ///Under line text            
                                    underlineColorAndroid='transparent'           
                                    style={styles.TextInputStyle}           
                                ></TextInput>
                                <TextInput       
                                    placeholder="Enter last name"        
                                    onChangeText={(text) => this.setState({last_name: text})}    
                                    underlineColorAndroid='transparent'       
                                    style={styles.TextInputStyle}     
                                ></TextInput>              
                                <TextInput            
                                    placeholder="Enter phone number"
                                    keyboardType = 'numeric'      
                                    onChangeText={(text) => this.setState({phone_number: text})}
                                    underlineColorAndroid='transparent'           
                                    style={styles.TextInputStyle}           
                                ></TextInput>         
                                <TextInput        
                                    placeholder="Enter email address"   
                                    onChangeText={(text) => this.setState({email: text})}
                                    underlineColorAndroid='transparent'       
                                    style={styles.TextInputStyle}     
                                ></TextInput>                  
                                <TextInput           
                                    placeholder="Enter password"              
                                    onChangeText={(text) => this.setState({password: text})}          
                                    underlineColorAndroid='transparent'       
                                    style={styles.TextInputStyle}         
                                    secureTextEntry={true}
                                ></TextInput>          
                                <TextInput           
                                    placeholder="Password comfirmation"            
                                    onChangeText={(text) => this.setState({c_password: text})}          
                                    underlineColorAndroid='transparent'       
                                    style={styles.TextInputStyle}         
                                    ///Hide password              
                                    secureTextEntry={true}
                                ></TextInput>
                                <TouchableOpacity 
                                    onPress={this.verification.bind(this)}
                                    style={styles.button}
                                >
                                    <Text style={styles.buttonText}>Sign Up</Text>
                                </TouchableOpacity>
                                <View>
                                    <Text 
                                        onPress={() => Actions.signin()}
                                         style={styles.colorText}>
                                        Have an Account already? Sign In
                                    </Text>
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }

    verification(){
        if (this.state.first_name === ""&&this.state.last_name === ""&&this.state.phone_number === ""&&this.state.email === ""&&this.state.password === ""&&this.state.c_password === "") {
            ToastAndroid.show('Please fill all fields!', ToastAndroid.SHORT);
        } else if (this.state.first_name === "") {
            ToastAndroid.show('The first name is empty!', ToastAndroid.SHORT);
        } else if (this.state.last_name === "") {
            ToastAndroid.show('The last name is empty!', ToastAndroid.SHORT);
        } else if (this.state.phone_number === "") {
            ToastAndroid.show('The phone number is empty!', ToastAndroid.SHORT);
        } else if (this.state.email === "") {
            ToastAndroid.show('The email is empty!', ToastAndroid.SHORT);
        } else if (this.state.password === "") {
            ToastAndroid.show('The password is empty!', ToastAndroid.SHORT);
        } else if (this.state.c_password === "") {
            ToastAndroid.show('The password comfirmation is empty!', ToastAndroid.SHORT);
        } else if (this.state.first_name.length < 2) {
            ToastAndroid.show('First name must contains at least 2 characters!', ToastAndroid.SHORT);
        } else if (this.state.last_name.length < 2) {
            ToastAndroid.show('Last name must contains at least 2 characters!', ToastAndroid.SHORT);
        } else if (this.state.phone_number.length < 9) {
            ToastAndroid.show('Phone number must contains at least 9 characters!', ToastAndroid.SHORT);
        } else if (!this.verifyEmail(this.state.email)) {
            ToastAndroid.show('Please enter a valid email address!', ToastAndroid.SHORT);
        } else if (this.state.password.length < 6) {
            ToastAndroid.show('Passwords must contains at least 6 characters!', ToastAndroid.SHORT);
        } else if (this.state.password !== this.state.c_password) {
            ToastAndroid.show('Passwords does not match!', ToastAndroid.SHORT);
        }else {
            this.onSignup();
        }
    }

    onSignup() {

        return fetch('http://192.168.1.123:8000/api/register', {  
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                phone_number: this.state.phone_number,
                email: this.state.email,
                password: this.state.password,
                c_password: this.state.c_password,
            })
        })
        .then((response) => response.json())
        .then(response => {
            if (response.status === true) {
                ToastAndroid.show('Success!', ToastAndroid.SHORT);
                Actions.signin();
            } else {
                ToastAndroid.show('Something was wrong. Please retry to Sign Up!', ToastAndroid.SHORT);
            }
        })
        .catch (error => {
            ToastAndroid.show(error, ToastAndroid.SHORT);
        });
    }
    verifyEmail (email) {
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return reg.test(email);
    }
}

const styles = StyleSheet.create({	
    container :{ 
      	justifyContent: 'center',
        backgroundColor: 'transparent',
      	flex:1,
      	paddingLeft: 10,	
      	paddingRight: 10,
    },
        content: {
        marginBottom: 20,
        alignItems: 'center',
    },  
    text: {
        fontSize: 20,
        marginTop: 20,
    },
    colorText: {
        color: '#8E8E8E',
        marginTop: 5,
        alignSelf: 'center',
    },
    TextInputStyle: { 		
      	textAlign: 'left',	
      	paddingLeft: 10,
      	paddingRight: 10,
    	marginBottom: 10,
      	height: 40,
        backgroundColor: '#FFFFFF',
      	borderWidth: 1,
      	borderColor: '#2196F3',
      	borderRadius: 8,
    },
    button: {
      	borderRadius: 8,
      	height: 40,
        backgroundColor: '#2196F3',
      	justifyContent: 'center'
    },
    buttonText: {
      	color: '#FFFFFF',
      	alignSelf: 'center',
      	fontSize: 14,
    },
});
