import React, { Component } from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity , Text, ToastAndroid } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';

var STORAGE_KEY = 'id_token';

export default class SignIn extends Component {
	
  	constructor() {
		super();
		this.state = {
		 	email: '',
		  	password: '',
		};
  	}

  	render() {
		var left = (
	      	<Left style={{flex:1}}>
	        	<Button onPress={() => this._sideMenuDrawer.open()} transparent>
	          		<Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
	        	</Button>
	      	</Left>
	    );

		return (
		    <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
		        <Container>
		           	<Navbar left={left} title="Sign In" />
		            <Content>
		              	<View style={styles.container}>

						  	<View style={styles.content}>
						  	  	<Text style={styles.text}>Login to your account !!!</Text>
						  	</View>	

							<TextInput			  
							  	placeholder="Enter email address"	  
							  	onChangeText={(text) => this.setState({email: text})}
								underlineColorAndroid='transparent'			  
								style={styles.TextInputStyle}			
							></TextInput>

							<TextInput 			 
								placeholder="Enter password"			  
								onChangeText={(text) => this.setState({password: text})}		  
								underlineColorAndroid='transparent'		  
								style={styles.TextInputStyle}		  
								//Hide password			  
								secureTextEntry={true}
							></TextInput>

							<TouchableOpacity 
							  	onPress={() => Actions.userHome()}
							  	//onPress={this.verification.bind(this)}
							  	style={styles.button}
							>
							  	<Text style={styles.buttonText}>Sign In</Text>
							</TouchableOpacity>

							<Text 
								onPress={() => Actions.signup()}
								style={styles.colorText}
							>
								Create a new account? Sign Up
							</Text>
						</View>
		            </Content>
		        </Container>
		    </SideMenuDrawer>
		);
	}

	verification(){
		if (this.state.email === "") {
	      	ToastAndroid.show('The email address is empty!');
	    } else if (this.state.password === "") {
	      	ToastAndroid.show('The password is empty!');
	    } else if (this.state.email === ""&&this.state.password === "") {
            ToastAndroid.show('Please fill all fields!', ToastAndroid.SHORT);
	    } else {
	    	this.onSignin();
	    }
	}

	onSignin() {
		return fetch('http://192.168.1.123:8000/api/login', {  
		  	method: 'POST',
		  	headers: {
		    	'Accept': 'application/json',
		    	'Content-Type': 'application/json',
		  	},
		  	body: JSON.stringify({
		    	email: this.state.email,
		    	password: this.state.password,
		  	})
		})
		.then((response) => response.json())
		.then(response => {
	    	if (response.status === true) {
	      		ToastAndroid.show('Success!', ToastAndroid.SHORT);
          		Actions.userHome();
	    	} else {
	      		ToastAndroid.show('Something went wrong on email or password!', ToastAndroid.SHORT);
	      	}
	  	})
	  	.catch(error => {
	      	ToastAndroid.show(error, ToastAndroid.SHORT);
	  	});	
	}
}

const styles = StyleSheet.create({	
  	container :{ 
		justifyContent: 'center',
        backgroundColor: 'transparent',
		flex:1,
		paddingLeft: 10,	
		paddingRight: 10,
 	},
  	content: {
	    marginBottom: 20,
	    alignItems: 'center',
  	},		
  	text: {
  	  	fontSize: 20,
  	  	marginTop: 20,
  	},
    colorText: {
        color: '#8E8E8E',
        alignSelf: 'center',
        marginTop: 5,
    },
	TextInputStyle: { 		
		textAlign: 'left',	
		paddingLeft: 10,
		paddingRight: 10,
		marginBottom: 10,
		height: 40,
		backgroundColor: '#FFFFFF',
		borderWidth: 1,
		borderColor: '#2196F3',
		borderRadius: 8,
	},
	button: {
		borderRadius: 8,
		height: 40,
		backgroundColor: '#2196F3',
		justifyContent: 'center'
	},
	buttonText: {
		color: '#FFFFFF',
		alignSelf: 'center',
		fontSize: 14,
	},
	error: {
	    color: 'red',
	    paddingTop: 10,
  	},
	success: {
	    color: 'green',
	    paddingTop: 10,
	},
	loader: {
	    marginTop: 20,
  	},
});

