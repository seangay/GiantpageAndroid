import React, { Component } from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Text } from 'react-native';

export default class Search extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TextInput     
                    placeholder="Search service like hotel, resorts ..."
                    underlineColorAndroid='transparent'       
                    style={styles.textSearch}     
                ></TextInput>
                <TouchableOpacity
                    onPress={() => this.onPress}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Search</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 10,
    },
    textSearch: {
        height: 40,
        padding: 5,
        marginRight: 5,
        backgroundColor: '#FFFFFF',
        flexGrow: 1,
        borderWidth: 2,
        borderRadius: 8,
        borderColor: '#2196F3',
    },
    button: {
        backgroundColor: '#2196F3',
        width: 60,
        height: 40,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#2196F3',
    },
    buttonText: {
        color: '#FFFFFF',
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 8,
    },
});


// import React, { Component } from 'react';

// import { Text, StyleSheet, View, ListView, TextInput, ActivityIndicator, Alert } from 'react-native';

// export default class Search extends Component {
 
//   constructor(props) {

//     super(props);

//     this.state = {

//       isLoading: true,
//       text: '',
    
//     }

//     this.arrayholder = [] ;
//   }
 
//   componentDidMount() {
 
//     return fetch('http://192.168.1.128:8000/api/search')
//       .then((response) => response.json())
//       .then((responseJson) => {
//         let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
//         this.setState({
//           isLoading: false,
//           dataSource: ds.cloneWithRows(responseJson),
//         }, function() {

//           // In this block you can do something with new state.
//           this.arrayholder = responseJson ;

//         });
//       })
//       .catch((error) => {
//         console.error(error);
//       });
      
//   }

//   GetListViewItem (fruit_name) {
    
//    Alert.alert(fruit_name);
  
//   }
  
//    SearchFilterFunction(text){
     
//      const newData = this.arrayholder.filter(function(item){
//          const itemData = item.fruit_name.toUpperCase()
//          const textData = text.toUpperCase()
//          return itemData.indexOf(textData) > -1
//      })
//      this.setState({
//          dataSource: this.state.dataSource.cloneWithRows(newData),
//          text: text
//      })
//  }
 
//   ListViewItemSeparator = () => {
//     return (
//       <View
//         style={{
//           height: .5,
//           width: "100%",
//           backgroundColor: "#000",
//         }}
//       />
//     );
//   }
 
 
//   render() {
//     if (this.state.isLoading) {
//       return (
//         <View style={{flex: 1, paddingTop: 20}}>
//           <ActivityIndicator />
//         </View>
//       );
//     }
 
//     return (
 
//       <View style={styles.MainContainer}>

//       <TextInput 
//        style={styles.TextInputStyleClass}
//        onChangeText={(text) => this.SearchFilterFunction(text)}
//        value={this.state.text}
//        underlineColorAndroid='transparent'
//        placeholder="Search Here"
//         />
 
//         <ListView
 
//           dataSource={this.state.dataSource}
 
//           renderSeparator= {this.ListViewItemSeparator}
 
//           renderRow={(rowData) => <Text style={styles.rowViewContainer} 

//           onPress={this.GetListViewItem.bind(this, rowData.fruit_name)} >{rowData.fruit_name}</Text>}

//           enableEmptySections={true}

//           style={{marginTop: 10}}
 
//         />
 
//       </View>
//     );
//   }
// }
 
// const styles = StyleSheet.create({
 
//  MainContainer :{

//   justifyContent: 'center',
//   flex:1,
//   margin: 7,
 
//   },
 
//  rowViewContainer: {
//    fontSize: 17,
//    padding: 10
//   },

//   TextInputStyleClass:{
        
//    textAlign: 'center',
//    height: 40,
//    borderWidth: 1,
//    borderColor: '#009688',
//    borderRadius: 7 ,
//    backgroundColor : "#FFFFFF"
        
//    }
 
// });
