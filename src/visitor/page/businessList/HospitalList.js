import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView, View, Text, Image } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Lightbox from 'react-native-lightbox';

import Communications from 'react-native-communications';

import Navbar from './../../component/Navbar';
import SideMenu from './../../component/SideMenu';
import SideMenuDrawer from './../../component/SideMenuDrawer';

export default class HospitalList extends Component {
	
    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="All Business" />
                    <Content>
                        <ScrollView style={styles.content}>
							<View style={styles.textShow}>
								<Text>There is have no business in this category</Text>
							</View>
                        </ScrollView>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        backgroundColor: '#e5e9ec',
    },
    textContent: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#333333',
        marginTop: 5,
        alignSelf: 'center',
    },
    textDetail: {
        color: '#636F7B',
        alignSelf: 'center',
    },
    background: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
    },
    col: {
        flexDirection: 'column',
    },
    img: {
        width: '100%',
        height: 180,
        marginTop: 10,
        paddingLeft: 5,
        paddingRight: 5,
    },
    text: {
        fontSize: 16,
        paddingLeft: 5,
        marginTop: 5,
    },
    textRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    address: {
        color: '#A29D9E',
        marginLeft: 5,

    },
    phone: {
        color: '#A29D9E',
        marginTop: 5,
        marginLeft: 8,
    },
    mail: {
        color: '#A29D9E',
        marginTop: 3,
        marginLeft: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    textButton1: {
        marginLeft: 5,
        justifyContent: 'center',
        alignSelf: 'center',

    },
    textButton2: {
        marginTop: 3,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    button1: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        marginLeft: 5,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
    button2: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        marginLeft: 10,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
	textShow: {
		marginTop: 20,
		alignItems: 'center',
	}
});
