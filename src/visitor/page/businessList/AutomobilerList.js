import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView, View, Text, Image } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Lightbox from 'react-native-lightbox';

import Communications from 'react-native-communications';

import Navbar from './../../component/Navbar';
import SideMenu from './../../component/SideMenu';
import SideMenuDrawer from './../../component/SideMenuDrawer';

export default class AutomobilerList extends Component {
    
    constructor() {
        super();
        this.state = {
			address: '#13D, street 371, Songkat Beuong Tumpon, Khan Mean Chey, Phnom Penh',
            name: 'HGB Auto',
            number: '012 345 678',
            email: 'hgb@gmail.com',
        };
    }

    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="All Business" />
                    <Content>
                        <ScrollView style={styles.content}>
                            <View>
                                <Text style={styles.textContent}>All Business</Text>
                            </View>
                            <View style={styles.background}>
                            <Lightbox>
                                <Image
                                    source = {require('./../../image/listCategory/6.jpeg')}
                                    style={styles.img}
                                ></Image>
                            </Lightbox>
                                <Text style={styles.text}>{this.state.name}</Text>
                                <View style={styles.textRow}>
                                    <Text>Adress:</Text>
                                    <Text style={styles.address}>{this.state.address}</Text>
                                </View>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-call-outline'></Icon>
                                    <Text style={styles.phone}> {this.state.number}</Text>
                                </View>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-mail-outline'></Icon>
                                    <Text style={styles.mail}>{this.state.email}</Text>
                                </View>
                                <View style={styles.buttonRow}>
                                    <TouchableOpacity
                                        onPress={() => Communications.phonecall(this.state.number, true)}
                                        style={styles.button1}>
                                        <View style={styles.textRow}>
                                            <Icon active name='ios-call-outline'/>
                                            <Text style={styles.textButton1}>Call Now</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => Communications.text(this.state.number)}
                                        style={styles.button2}>
                                        <View style={styles.textRow}>
                                            <Icon active name='ios-chatbubbles-outline'/>
                                            <Text style={styles.textButton1}>Send SMS</Text>
                                        </View>
                                     </TouchableOpacity>
                                </View>
                                <View style={styles.buttonRow}>
                                    <TouchableOpacity
                                        onPress={()=>{}}
                                        style={styles.button1}>
                                        <View style={styles.textRow}>
                                            <Icon 
                                                name='pencil'
                                                type='EvilIcons'
                                            ></Icon>
                                            <Text style={styles.textButton2}>Write Review</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={()=>{}}
                                        style={styles.button2}>
                                        <View style={styles.textRow}>
                                            <Icon 
                                                name='pencil'
                                                type='EvilIcons'
                                            ></Icon>
                                            <Text style={styles.textButton2}>Get Quotes</Text>
                                        </View>
                                     </TouchableOpacity>
                                </View>
                                <View style={styles.buttonRow}>
                                    <TouchableOpacity
                                        onPress={()=>Actions.signin()}
                                        style={styles.button1}>
                                        <View style={styles.textRow}>
                                            <Icon active name='ios-heart-outline'/>
                                            <Text style={styles.textButton1}>Favorite</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={()=>Actions.businessDetail()}
                                        style={styles.button2}>
                                        <View style={styles.textRow}>
                                            <Icon 
                                                name='ios-more-outline'
                                                type='Ionicons'
                                            ></Icon>
                                            <Text style={styles.textButton1}>View Detail</Text>
                                        </View>
                                     </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        backgroundColor: '#e5e9ec',
    },
    textContent: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#333333',
        marginTop: 5,
        alignSelf: 'center',
    },
    textDetail: {
        color: '#636F7B',
        alignSelf: 'center',
    },
    background: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
    },
    col: {
        flexDirection: 'column',
    },
    img: {
        width: '100%',
        height: 180,
        marginTop: 10,
        paddingLeft: 5,
        paddingRight: 5,
    },
    text: {
        fontSize: 16,
        paddingLeft: 5,
        marginTop: 5,
    },
    textRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    address: {
        color: '#A29D9E',
        marginLeft: 5,

    },
    phone: {
        color: '#A29D9E',
        marginTop: 5,
        marginLeft: 8,
    },
    mail: {
        color: '#A29D9E',
        marginTop: 3,
        marginLeft: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    textButton1: {
        marginLeft: 5,
        justifyContent: 'center',
        alignSelf: 'center',

    },
    textButton2: {
        marginTop: 3,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    button1: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        marginLeft: 5,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
    button2: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        marginLeft: 10,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
});
