import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, Image } from 'react-native';
import { View, List, ListItem, Body, Icon, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';


export default class SideMenu extends Component {
    render() {
        return(

            <ScrollView style={styles.container}>
                <View style={{paddingRight: 15}}>
                    <List>
                        <ListItem
                            Icon = ''
                            key={'home'}
                            Button={true}
                            onPress={() => Actions.home()}
                        >
                            <Body style={styles.row}>
                                <Icon
                                    name='ios-home'
                                    type='Ionicons'
                                    style={size=30}
                                ></Icon>
                                <Text style={styles.text}>Home</Text>
                            </Body>
                        </ListItem>

                        
                        <ListItem
                            Icon= ''
                            key = {'signin'}
                            Button={true}
                            onPress={() => Actions.signin()}
                        >
                            <Body style={styles.row}>
                                <Icon
                                    name='ios-person'
                                    type='Ionicons'
                                    style={size=30}
                                ></Icon>
                                <Text style={styles.text1}>Sign In</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'signup'}
                            Button={true}
                            onPress={() => Actions.signup()}
                        >
                            <Body style={styles.row}>
                                <Icon
                                    name='ios-person-add'
                                    type='Ionicons'
                                    style={size=30}
                                ></Icon>
                                <Text style={styles.text1}>Sign Up</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            key={'categories'}
                            Button={false}
                        >
                            <Body>
                                <Text style={styles.textContent}>ALL CATEGORIES</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'userListCategory'}
                            Button={true}
                            onPress={() => Actions.automobiler()}
                        >
                            <Body style={styles.textRow2}>
                                <Icon
                                    name='ios-car'
                                    type='Ionicons'
                                ></Icon>
                                <Text style={styles.text2}>Automobiler</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.education()}
                        >
                            <Body style={styles.textRow2}>
                                <Icon
                                    name='book'
                                    type='Entypo'
                                ></Icon>
                                <Text style={styles.text2}>Education</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.electricals()}
                        >
                            <Body style={styles.textRow2}>
                                <Image
                                    source = {require('./../image/electrice.png')}
                                    style={styles.img2}
                                ></Image>
                                <Text style={styles.text1}>Electricals</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.hospitals()}
                        >
                            <Body style={styles.textRow1}>
                                <Icon
                                    name='hospital-o'
                                    type='FontAwesome'
                                ></Icon>
                                <Text style={styles.text3}>Hospitals</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.hotelsResorts()}
                        >
                            <Body style={styles.textRow}>
                                <Image
                                    source = {require('./../image/hotel.png')}
                                    style={styles.img}
                                ></Image>
                                <Text style={styles.text2}>Hotels & Resorts</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.property()}
                        >
                            <Body style={styles.textRow}>
                                <Image
                                    source = {require('./../image/building.png')}
                                    style={styles.img1}
                                ></Image>
                                <Text style={styles.text2}>Property</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.sport()}
                        >
                            <Body style={styles.textRow1}>
                                <Icon
                                    name='ios-football'
                                    type='Ionicons'
                                ></Icon>
                                <Text style={styles.text3}>Sport</Text>
                            </Body>
                        </ListItem>

                        <ListItem
                            Icon= ''
                            key = {'listCategory'}
                            Button={true}
                            onPress={() => Actions.transportation()}
                        >
                            <Body style={styles.textRow1}>
                                <Image
                                    source = {require('./../image/transport.png')}
                                    style={styles.img2}
                                ></Image>
                                <Text style={styles.text1}>Transportation</Text>
                            </Body>
                        </ListItem>
                    </List>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e5e9ec',
    },
    textRow: {
        flexDirection: 'row',
        marginLeft: 17,
    },
    textRow1: {
        flexDirection: 'row',
        marginLeft: 23,
    },
    textRow2: {
        flexDirection: 'row',
        marginLeft: 20,
    },
    row: {
        flexDirection: 'row',
    },
    textContent: {
        fontSize: 18,
    },
    text: {
        color: '#233A53',
        marginLeft: 5,
        marginTop: 5,
    },
    text1: {
        color: '#233A53',
        marginLeft: 10,
        marginTop: 5,
    },
    text2: {
        color: '#233A53',
        marginLeft: 20,
        marginTop: 5,
    },
    text3: {
        color: '#233A53',
        marginLeft: 25,
        marginTop: 5,
    },
    img: {
        width: 35,
        height: 35,
    },
    img1: {
        width: 35,
        height: 35,
    },
    img2: {
        width: 25,
        height: 25,
        marginRight: 15,
    },
});
