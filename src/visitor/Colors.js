const Colors = {
  	navbarBackgroundColor: '#2196F3',
  	statusBarColor: '#233240'
};

export default Colors;
