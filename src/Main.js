import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { Root } from 'native-base';
import { Scene, Router, Actions } from 'react-native-router-flux';

import Home from './visitor/page/Home';
import SignUp from './visitor/page/SignUp';
import SignIn from './visitor/page/SignIn';
import BusinessCategory from './visitor/page/BusinessCategory';
import AutomobilerList from './visitor/page/businessList/AutomobilerList';
import EducationtList from './visitor/page/businessList/EducationtList';
import ElectricalsList from './visitor/page/businessList/ElectricalsList';
import HospitalList from './visitor/page/businessList/HospitalList';
import HotelsAndResortList from './visitor/page/businessList/HotelsAndResortList';
import PropertyList from './visitor/page/businessList/PropertyList';
import SportList from './visitor/page/businessList/SportList';
import TransportationList from './visitor/page/businessList/TransportationList';
import BusinessDetail from './visitor/page/BusinessDetail';
import UserHome from './user/page/Home';
import UserBusinessCategory from './user/page/BusinessCategory';
import UserAutomobilerList from './user/page/businessList/AutomobilerList';
import UserEducationtList from './user/page/businessList/EducationtList';
import UserElectricalsList from './user/page/businessList/ElectricalsList';
import UserHospitalList from './user/page/businessList/HospitalList';
import UserHotelsAndResortList from './user/page/businessList/HotelsAndResortList';
import UserPropertyList from './user/page/businessList/PropertyList';
import UserSportList from './user/page/businessList/SportList';
import UserTransportationList from './user/page/businessList/TransportationList';
import UserCreateBusinessDetail from './user/page/BusinessDetail';
import UserProfile from './user/page/Profile';
import UserChangePassword from './user/page/ChangePassword';
import UserProfileUpdateInfo from './user/page/ProfileUpdateInfo';
import UserProfileInfo from './user/page/ProfileInfo';
import UserFavorite from './user/page/Favorite';


export default class Main extends Component {
    componentWillMount = () => {
        BackHandler.addEventListener('hardwareBackPress', () => Actions.pop());
    };

    render() {
        return(
            <Root>
                <Router>
                    <Scene key="root">
                        <Scene initial key="home" component={Home} hideNavBar />
                        <Scene key="signup" component={SignUp} modal hideNavBar />
                        <Scene key="signin" component={SignIn} modal hideNavBar />
                        <Scene key="categories" component={BusinessCategory} modal hideNavBar />
                        <Scene key="automobiler" component={AutomobilerList} modal hideNavBar />
                        <Scene key="education" component={EducationtList} modal hideNavBar />
                        <Scene key="electricals" component={ElectricalsList} modal hideNavBar />
                        <Scene key="hospitals" component={HospitalList} modal hideNavBar />
                        <Scene key="hotelsResorts" component={HotelsAndResortList} modal hideNavBar />
                        <Scene key="property" component={PropertyList} modal hideNavBar />
                        <Scene key="sport" component={SportList} modal hideNavBar />
                        <Scene key="transportation" component={TransportationList} modal hideNavBar />
                        <Scene key="businessDetail" component={BusinessDetail} hideNavBar />
                        <Scene key="userHome" component={UserHome} modal hideNavBar />
                        <Scene key="userCategories" component={UserBusinessCategory} modal hideNavBar />
                        <Scene key="userAutomobiler" component={UserAutomobilerList} modal hideNavBar />
                        <Scene key="userEducation" component={UserEducationtList} modal hideNavBar />
                        <Scene key="userElectricals" component={UserElectricalsList} modal hideNavBar />
                        <Scene key="userHospitals" component={UserHospitalList} modal hideNavBar />
                        <Scene key="userHotelsResorts" component={UserHotelsAndResortList} modal hideNavBar />
                        <Scene key="userProperty" component={UserPropertyList} modal hideNavBar />
                        <Scene key="userSport" component={UserSportList} modal hideNavBar />
                        <Scene key="userTransportation" component={UserTransportationList} modal hideNavBar />
                        <Scene key="userBusinessDetail" component={UserCreateBusinessDetail} hideNavBar />
                        <Scene key="userProfile" component={UserProfile} modal hideNavBar />
                        <Scene key="userChangePassword" component={UserChangePassword} modal hideNavBar />
                        <Scene key="userProfileUpdateInfo" component={UserProfileUpdateInfo} hideNavBar />
                        <Scene key="userProfileInfo" component={UserProfileInfo} hideNavBar />
                        <Scene key="userFavorite" component={UserFavorite} hideNavBar />
                    </Scene>
                </Router>
            </Root>
        );
    }
}
