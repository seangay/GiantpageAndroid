import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

export default class ProfileInfo extends Component {
	
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    showMenu = () => {
        this._menu.show();
    };

    render () {
		return(
            <View style= {styles.Container}>
                <Menu
                    ref={this.setMenuRef}
                    button={<Text onPress={this.showMenu}>Show menu</Text>}
                >
                    <MenuItem onPress={() => Actions.userProfileUpdateInfo()}>Update Info</MenuItem>
                    <MenuItem onPress={() => Actions.userChangePassword()}>Change Password</MenuItem>
                    <MenuItem onPress={() => Actions.home()}>Log Out</MenuItem>
                </Menu>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});