import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView, View, Text, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Search from './../../visitor/page/Search';

export default class BusinessCategory extends Component {
    render() {
        return (
            <ScrollView style={styles.content}>
                <Search />
                <View style={styles.contentText}>
                    <Text style={styles.textContent}>Find Your Services</Text>
                </View>

                <TouchableOpacity
                    onPress={() => Actions.automobiler()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/1.jpeg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Automobiler</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.education()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/2.jpeg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Education</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.electricals()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/3.jpeg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Electricals</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.hospitals()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/4.jpg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Hospitals</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.hotelsResorts()}
                    style={styles.background}>    
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/5.jpg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Hotels and Resorts</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.property()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/6.jpeg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Property</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.sport()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/7.jpeg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Sport</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => Actions.transportation()}
                    style={styles.background}>  
                    <View style={styles.col}>
                        <Image
                            source = {require('./../image/category/8.jpg')}
                            style={styles.img}
                        ></Image>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>Transportation</Text>
                            <Text style={styles.textHin}>Show all</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
  	content: {
        flex: 1,
      	paddingRight: 10,
      	paddingLeft: 10,
        backgroundColor: '#e5e9ec',
    },
    contentText: {
        marginBottom: 10,
    },
    textContent: {
        fontSize: 22,
        fontWeight: 'bold',
        color: '#333333',
        marginTop: 5,
        alignSelf: 'center',
    },
    textDetail: {
        color: '#636F7B',
        alignSelf: 'center',
    },
    background: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
    },
    col: {
        flexDirection: 'column',
    },
    img: {
        width: '100%',
      	height: 180,
    },
    text: {
        fontSize: 16,
        paddingLeft: 5,
    },
    textRow: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    textHin: {
        color: '#A29D9E',
        marginTop: 3,
        right:  5,
        position: 'absolute',
    },
});
