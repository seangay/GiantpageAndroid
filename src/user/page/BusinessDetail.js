import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView, View, Text, ImageBackground } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Lightbox from 'react-native-lightbox';

import Communications from 'react-native-communications';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';
import BusinessServiceDetail from'./../../user/page/BusinessServiceDetail';
import BusinessRoomBooking from'./../../user/page/BusinessRoomBooking';

export default class BusinessDetail extends Component {
    constructor() {
        super();
        this.state = {
            address: '#13D, street 371, Songkat Beuong Tumpon, Khan Mean Chey, Phnom Penh',
            name: 'Taj Luxury Hotel & Resorts',
            number: '012 345 678',
            email: 'san@gmail.com',
        };
    }

    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="Business Ditail" />
                    <Content>
                        <ScrollView>
                            <ImageBackground
                                source = {require('./../image/businessDetail/1.jpg')}
                                style={styles.img}
                            >
                                <View style={styles.profile}>
                                    <Text style={styles.text}>{this.state.name}</Text>
                                    <View style={styles.textRow}>
                                        <Text style={styles.icon}>Adress:</Text>
                                        <Text style={styles.address}>{this.state.address}</Text>
                                    </View>
                                    <View style={styles.textRow}>
                                        <Icon
                                            name='ios-call-outline'
                                            type='Ionicons'
                                            style={styles.icon}
                                        ></Icon>
                                        <Text style={styles.phone}>{this.state.number}</Text>
                                    </View>
                                    <View style={styles.textRow}>
                                        <Icon
                                            name='ios-mail-outline'
                                            type='Ionicons'
                                            style={styles.icon}
                                        ></Icon>
                                        <Text style={styles.mail}>{this.state.email}</Text>
                                    </View>
                                    <View style={styles.buttonRow}>
                                        <TouchableOpacity
                                            onPress={() => Communications.phonecall(this.state.number, true)}
                                            style={styles.button1}>
                                            <View style={styles.textRow}>
                                                <Icon
                                                    name='ios-call-outline'
                                                    type='Ionicons'
                                                ></Icon>
                                                <Text style={styles.textButton1}>Call Now</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => Communications.text(this.state.number)}
                                            style={styles.button2}>
                                            <View style={styles.textRow}>
                                                <Icon
                                                    name='ios-chatbubbles-outline'
                                                    type='Ionicons'
                                                ></Icon>
                                                <Text style={styles.textButton1}>Send SMS</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.buttonRow}>
                                        <TouchableOpacity
                                            onPress={()=>{}}
                                            style={styles.button1}>
                                            <View style={styles.textRow}>
                                                <Icon 
                                                    name='pencil'
                                                    type='EvilIcons'
                                                ></Icon>
                                                <Text style={styles.textButton2}>Write Review</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => Communications.email([this.state.email],null,null,'My Subject','My body text')}
                                            style={styles.button2}>
                                            <View style={styles.textRow}>
                                                <Icon
                                                    name='pencil'
                                                    type='EvilIcons'
                                                ></Icon>
                                                <Text style={styles.textButton2}>Get Quotes</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View> 
                            </ImageBackground>

                            <View style={styles.container}>
                                <View style={styles.body1}>
                                    <View style={styles.textRow}>
                                        <Text style={styles.textTitleColor}>ABOUT</Text>
                                        <Text style={styles.textTitle}> TAJ LUXURY</Text>
                                    </View>
                                    <View
                                        style={{
                                            borderBottomColor: '#A6A8A6',
                                            borderBottomWidth: 1,
                                            marginBottom: 5,
                                        }}
                                    ></View>
                                    <View>
                                        <Text style={styles.textColor}>Taj Luxury Hotels & Resorts presents award winning luxury hotels and resorts in India, Indonesia, Mauritius, Egypt and Saudi Arabia.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution</Text>
                                        <Text style={styles.textColor}>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</Text>
                                    </View>
                                </View>
                                <View style={styles.body2}>
                                   <View style={styles.textRow}>
                                        <Text style={styles.textTitleColor}>SERVICES</Text>
                                        <Text style={styles.textTitle}> OFFERED</Text>
                                    </View>
                                    <View
                                        style={{
                                            borderBottomColor: '#A6A8A6',
                                            borderBottomWidth: 1,
                                            marginBottom: 5,
                                        }}
                                    ></View> 
                                    <View>
                                        <Text style={styles.textColor}>Taj Luxury Hotels & Resorts provide 24-hour Business Centre, Clinic, Internet Access Centre, Babysitting, Butler Service in Villas and Seaview Suite, House Doctor on Call, Airport Butler Service, Lobby Lounge.</Text>
                                    </View>
                                        <BusinessServiceDetail />
                                </View>
                                <View style={styles.body3}>
                                    <View style={styles.textRow}>
                                        <Text style={styles.textTitleColor}>ROOM</Text>
                                        <Text style={styles.textTitle}> BOOKING</Text>
                                    </View>
                                    <View
                                        style={{
                                            borderBottomColor: '#A6A8A6',
                                            borderBottomWidth: 1,
                                            marginBottom: 5,
                                        }}
                                    ></View>
                                    <BusinessRoomBooking />
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }
}

const styles = StyleSheet.create({
    img: {
        height: 230,
    },
    profile: {
        bottom:  0,
        position: 'absolute',
        backgroundColor: 'transparent'
    },
    icon: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginLeft: 10,
    },
    text: {
        fontSize: 22,
        fontWeight: 'bold',
        paddingLeft: 10,
        color: '#FFFFFF',
    },
    textRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    address: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginLeft: 5,

    },
    phone: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginTop: 5,
        marginLeft: 8,
    },
    mail: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginTop: 3,
        marginLeft: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    textButton1: {
        margin: 5,
    },
    textButton2: {
        marginTop: 5,
    },
    button1: {
        marginBottom: 10,
        marginLeft: 10,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
    button2: {
        marginBottom: 10,
        marginLeft: 13,
        borderWidth: 1,
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        width: 135,
        height: 30,
        alignItems: 'center',
    },
    container: {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        backgroundColor: 'transparent',
    },
    body1: {
        backgroundColor: '#FFFFFF',
        marginTop: 30,
        marginBottom: 10,
        padding: 5,
    },
    body2: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
        padding: 5,
    },
    body3: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
        marginBottom: 10,
        padding: 5,
    },
    textTitleColor: {
        color:'#A6A8A6',
        fontSize: 18,
        marginTop: 5,
        marginBottom: 10,
    },
    textTitle: {
        fontSize: 18,
        marginTop: 5,
        marginBottom: 10,
    },
    textColor: {
        color:'#A6A8A6',
    },
});
