import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, ScrollView, View, Text, Image } from 'react-native';
import { Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Lightbox from 'react-native-lightbox';

export default class BusinessRoomBooking extends Component {
    render() {
        return(
            <ScrollView>
                <View>
                    <View style={styles.background}>
                    <Lightbox>
                        <Image
                            source = {require('./../image/businessDetail/8.jpg')}
                            style={styles.img}
                        ></Image>
                    </Lightbox>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>ULTRA DELUXE ROOMS</Text>
                            <Text style={styles.textRight}>$940</Text>
                        </View>
                        <View style={styles.directionRow}>
                            <Text style={{backgroundColor: '#F5F5F5'}}>Amenities:</Text>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a1.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>wifi</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a2.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Air Conditioner</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a3.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Swimming Pool</Text>
                            </View>
                        </View>
                        <View style={styles.directionRow}>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a4.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Bar</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a5.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Bathroom</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a6.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>TV</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a7.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Spa</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a8.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Music</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a9.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Parking</Text>
                            </View>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-call-outline'/>
                                    <Text style={styles.textButton1}>Call Now</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-chatbubbles-outline'/>
                                    <Text style={styles.textButton1}>Send SMS</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon 
                                        name='pencil'
                                        type='EvilIcons'
                                    ></Icon>
                                    <Text style={styles.textButton2}>Get Quotes</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon
                                        name='dollar'
                                        type='Foundation'
                                    ></Icon>
                                    <Text style={styles.textButton1}>Book Now</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                    <View
                        style={{
                            borderBottomColor: '#A6A8A6',
                            borderBottomWidth: 1,
                            marginTop: 5,
                            marginBottom: 5,
                        }}
                    ></View>
                <View> 
                    <View style={styles.background}>
                    <Lightbox>
                        <Image
                            source = {require('./../image/businessDetail/9.jpg')}
                            style={styles.img}
                        ></Image>
                    </Lightbox>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>PREMIUM ROOMS(EXECUTIVE)</Text>
                            <Text style={styles.textRight}>$620</Text>
                        </View>
                        <View style={styles.directionRow}>
                             <Text style={{backgroundColor: '#F5F5F5'}}>Amenities:</Text>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a1.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>wifi</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a2.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Air Conditioner</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a3.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Swimming Pool</Text>
                            </View>
                        </View>
                        <View style={styles.directionRow}>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a4.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Bar</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a5.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Bathroom</Text>
                            </View>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-call-outline'/>
                                    <Text style={styles.textButton1}>Call Now</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-chatbubbles-outline'/>
                                    <Text style={styles.textButton1}>Send SMS</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon 
                                        name='pencil'
                                        type='EvilIcons'
                                    ></Icon>
                                    <Text style={styles.textButton2}>Get Quotes</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon
                                        name='dollar'
                                        type='Foundation'
                                    ></Icon>
                                    <Text style={styles.textButton1}>Book Now</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                    <View
                        style={{
                            borderBottomColor: '#A6A8A6',
                            borderBottomWidth: 1,
                            marginTop: 5,
                            marginBottom: 5,
                        }}
                    ></View>
                <View> 
                    <View style={styles.background}>
                    <Lightbox>
                        <Image
                            source = {require('./../image/businessDetail/10.jpg')}
                            style={styles.img}
                        ></Image>
                    </Lightbox>
                        <View style={styles.textRow}>
                            <Text style={styles.text}>NORMAL ROOMS(EXECUTIVE)</Text>
                            <Text style={styles.textRight}>$420</Text>
                        </View>
                        <View style={styles.directionRow}>
                            <Text style={{backgroundColor: '#F5F5F5'}}>Amenities:</Text>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a1.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>wifi</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a2.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Air Conditioner</Text>
                            </View>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a3.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Swimming Pool</Text>
                            </View>
                        </View>
                        <View style={styles.directionRow}>
                            <View style={styles.iconRow}>
                                <Image
                                    source = {require('./../image/businessDetail/a4.png')}
                                    style={styles.icon}
                                ></Image>
                                <Text style={styles.iconTitle}>Bar</Text>
                            </View>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-call-outline'/>
                                    <Text style={styles.textButton1}>Call Now</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon active name='ios-chatbubbles-outline'/>
                                    <Text style={styles.textButton1}>Send SMS</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonRow}>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button1}>
                                <View style={styles.textRow}>
                                    <Icon 
                                        name='pencil'
                                        type='EvilIcons'
                                    ></Icon>
                                    <Text style={styles.textButton2}>Get Quotes</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{}}
                                style={styles.button2}>
                                <View style={styles.textRow}>
                                    <Icon
                                        name='dollar'
                                        type='Foundation'
                                    ></Icon>
                                    <Text style={styles.textButton1}>Book Now</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>   
        );
    }
}

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#FFFFFF',
        marginTop: 10,
    },
    img: {
        width: '100%',
        height: 180,
        paddingLeft: 5,
        paddingRight: 5,
    },
    text: {
        fontSize: 16,
        marginTop: 10,
        marginBottom: 10,
    },
    textRight: {
        fontSize: 16,
        marginTop: 10,
        marginBottom: 10,
        right:  5,
        position: 'absolute',

    },
    directionRow: {
        flexDirection: 'row',
    },
    icon: {
        width: 20,
        height: 20,
    },
    iconTitle: {
        fontSize: 10,
        color: '#ACACAC',
        marginTop: 2,
    },
    iconRow: {
        flexDirection: 'row',
        backgroundColor: '#F5F5F5',
        marginTop: 5,
        marginLeft: 5,
        marginBottom: 5,
    },
    textRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    buttonRow: {
        flexDirection: 'row',
        paddingLeft: 5,
    },
    textButton1: {
        margin: 3,
    },
    textButton2: {
        marginTop: 3,
    },
    button1: {
        marginBottom: 10,
        marginTop: 5,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        width: 135,
        alignItems: 'center',
    },
    button2: {
        marginBottom: 10,
        marginTop: 5,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
        marginLeft: 5,
        width: 135,
        alignItems: 'center',
    },
});