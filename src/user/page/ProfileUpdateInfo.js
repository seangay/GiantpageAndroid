import React, { Component } from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity, Text } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';

export default class ProfileUpdateInfo extends Component {
 
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            phone_number: '',
            email: '',
            hasError: false,
            errorText: '',
        };
    }
      
    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="Update Profile" />
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.content}>
                                <Text style={styles.text}>Update your information </Text>
                            </View>
                            <TextInput
                                ///Hint text input        
                                placeholder="First name"        
                                onChangeText={(text) => this.setState({first_name: text})}        
                                ///Under line text        
                                underlineColorAndroid='transparent'       
                                style={styles.TextInputStyle}     
                            ></TextInput>
                            <TextInput        
                                placeholder="Last name"        
                                onChangeText={(text) => this.setState({last_name: text})}      
                                underlineColorAndroid='transparent'       
                                style={styles.TextInputStyle}     
                            ></TextInput>        
                            <TextInput        
                                placeholder="Phone number"
                                keyboardType = 'numeric'    
                                onChangeText={(text) => this.setState({phone_number: text})}
                                underlineColorAndroid='transparent'       
                                style={styles.TextInputStyle}     
                            ></TextInput>         
                            <TextInput        
                                placeholder="Email"   
                                onChangeText={(text) => this.setState({email: text})}
                                underlineColorAndroid='transparent'       
                                style={styles.TextInputStyle}     
                            ></TextInput>
                            <View>
                                <TouchableOpacity 
                                    onPress={() => this.verification()}
                                    //onPress={this.verification.bind(this)}
                                    style={styles.button}
                                >
                                    <Text style={styles.buttonText}>Save Change</Text>
                                </TouchableOpacity>

                                <TouchableOpacity 
                                    onPress={() => Actions.userProfile()}
                                    style={styles.button}
                                >
                                    <Text style={styles.buttonText}>Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }

    verification() {
        if (this.state.first_name === "") {
            ToastAndroid.show('The first name is empty!', ToastAndroid.LONG);
        } else if (this.state.last_name === "") {
            ToastAndroid.show('The last name is empty!', ToastAndroid.LONG);
        } else if (this.state.phone_number === "") {
            ToastAndroid.show('The phone number is empty!', ToastAndroid.LONG);
        } else if (this.state.email === "") {
            ToastAndroid.show('The email is empty!', ToastAndroid.LONG);
        } else if (this.state.first_name.length < 2) {
            ToastAndroid.show('First name must contains at least 2 characters!', ToastAndroid.LONG);
        } else if (this.state.last_name.length < 2) {
            ToastAndroid.show('Last name must contains at least 2 characters!', ToastAndroid.LONG);
        } else if (this.state.phone_number.length < 9) {
            ToastAndroid.show('Phone number must contains at least 9 characters!', ToastAndroid.LONG);
        } else if (!this.verifyEmail(this.state.email)) {
            ToastAndroid.show('Please enter a valid email address!', ToastAndroid.LONG);
        } else if (this.state.first_name === ""&&this.state.last_name === ""&&this.state.phone_number === ""&&this.state.email === "") {
            ToastAndroid.show('Please fill all fields!', ToastAndroid.LONG);
        } else {
            
        }
    }

    verifyEmail (email) {
        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return reg.test(email);
    }
}

const styles = StyleSheet.create({  
    container :{ 
        justifyContent: 'center',
        backgroundColor: 'transparent',
        flex:1,
        paddingLeft: 10,  
        paddingRight: 10,
    },
    content: {
        marginBottom: 20,
        alignItems: 'center',
    },  
    text: {
        fontSize: 20,
    },
    TextInputStyle: {     
        textAlign: 'left',  
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10,
        height: 40,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 8,
    },
    button: {
        borderRadius: 8,
        height: 40,
        backgroundColor: '#2196F3',
        justifyContent: 'center',
        marginBottom: 10,
    },
    buttonText: {
        color: '#FFFFFF',
        alignSelf: 'center',
        fontSize: 14,
    }
});
