import React, { Component } from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity, Text, ToastAndroid } from 'react-native';
import { Container, Content, Button, Left, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';
import Category from './BusinessCategory';

export default class ChangePassword extends Component {
	
  	constructor(props) {
		super(props);
		this.state = {
		  	current_password: '',
		  	new_password: '',
		  	password_confirmation: '',
		};
  	}
		  
  	render() {
  		var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} title="Giant Page" />
                    <Content>
                        <View style={styles.container}>
						  	<View style={styles.content}>
						  	  	<Text style={styles.text}>Change your password !!!</Text>
						  	</View>
							<TextInput 			 
							  	placeholder="Current Password"			  
							  	onChangeText={(text) => this.setState({current_password: text})}		  
							  	underlineColorAndroid='transparent'		  
							  	style={styles.TextInputStyle}		  
							  	secureTextEntry={true}
							></TextInput>
							
							<TextInput 			 
							  	placeholder="New Password"			  
							  	onChangeText={(text) => this.setState({new_password: text})}		  
							  	underlineColorAndroid='transparent'		  
							  	style={styles.TextInputStyle}		  
							  	secureTextEntry={true}
							></TextInput>	
							
							<TextInput 			 
							  	placeholder="Password Confirmation"			  
							  	onChangeText={(text) => this.setState({password_confirmation: text})}		  
							  	underlineColorAndroid='transparent'		  
							  	style={styles.TextInputStyle}			  
							  	secureTextEntry={true}
							></TextInput>
							
							<TouchableOpacity 
							  	onPress={this.verification.bind(this)}
							  	style={styles.button1}>
							  	<Text style={styles.buttonText}>Save Change</Text>
							</TouchableOpacity>
							
							<TouchableOpacity 
							  	onPress={() => Actions.userProfile()}
							  	style={styles.button2}>
							  	<Text style={styles.buttonText}>Cancel</Text>	
							</TouchableOpacity>
					  	</View>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }
  	
    verification(){
    	if (this.state.current_password === "") {
    		ToastAndroid.show('The current password is empty!');
    	} else if (this.state.new_password === "") {
    		ToastAndroid.show('The new password is empty!');
    	} else if (this.state.password_confirmation === "") {
    		ToastAndroid.show('The password confirmation is empty!');
    	} else if (this.state.current_password == this.state.new_password) {
    		ToastAndroid.show('The new password and current password must be different!');
    	}else if (this.state.new_password.length < 6) {
    		ToastAndroid.show('Passwords must contains at least 6 characters!', ToastAndroid.SHORT);
    	} else if (this.state.new_password !== this.state.password_confirmation) {
    		ToastAndroid.show('Passwords does not match!', ToastAndroid.SHORT);
    	} else if (this.state.current_password === "" && this.state.new_password === "" && this.state.password_confirmation === "") {
    		ToastAndroid.show ('Please fill all fields');
    	} else {
    		this.changePassword();
    	}
    }

  	changePassword(){
		return fetch('http://192.168.1.123:8000/api/changepassword', {  
		  	method: 'POST',
		  	headers: {
		    	'Accept': 'application/json',
		    	'Content-Type': 'application/json',
		  	},
		  	body: JSON.stringify({
		    	current_password: this.state.current_password,
		    	new_password: this.state.new_password,
		    	password_confirmation: this.state.password_confirmation,
		  	})
		})
		.then((response) => response.json())
		.then(response => {
	    	if (response.status === true) {
	      		ToastAndroid.show('Success!', ToastAndroid.SHORT);
          		Actions.userProfile();
	    	} else {
	      		ToastAndroid.show('Something went wrong. Please retry to change your password!', ToastAndroid.SHORT);
	    	}
	  	})
	  	.then(response => {
	    	console.debug(response);
	  	})
	  	.catch(error => {
	      	ToastAndroid.show(error, ToastAndroid.SHORT);
	  	});	
	}

}

const styles = StyleSheet.create({	
  	container :{ 
	    justifyContent: 'center',
        backgroundColor: 'transparent',
		flex:1,
		paddingLeft: 10,	
		paddingRight: 10,
  	},
  	content: {
	    marginBottom: 20,
	    alignItems: 'center',
  	},  
  	text: {
    	fontSize: 20,
    	marginTop: 20,
  	},	
  	TextInputStyle: { 		
		textAlign: 'left',	
		paddingLeft: 10,
		paddingRight: 10,
		marginBottom: 10,
		height: 40,
		backgroundColor: '#FFFFFF',
		borderWidth: 1,
		borderColor: '#2196F3',
		borderRadius: 8,
  	},
  	button1: {
		borderRadius: 8,
		height: 40,
		backgroundColor: '#2196F3',
		justifyContent: 'center'
  	},
  	button2: {
		marginTop: 10,
		borderRadius: 8,
		height: 40,
		backgroundColor: '#2196F3',
		justifyContent: 'center'
  	},
  	buttonText: {
		color: '#FFFFFF',
		alignSelf: 'center',
		fontSize: 14,
  	}
});
