import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';


export default class Favorite extends Component {
	constructor(props){
		super(props);
		this.state={
			dataSource: []
		}
	}

	renderItem = ({ item }) => {
		return(
			<View>
				<Image
					source={{url: item.user_image}}
				></Image>
				<View>
					<Text>
						{item.user_first_name+" "+item.user_last_name}
					</Text>
				</View>
			</View>
		)
	}

	compnentDidMount(){
		const url = 'http://192.168.1.123:8000/api/get-details';
		fetch(url)
		.then((response) =>response.json())
		.then((responseJson) =>{
			this.setState({
				dataSource: responseJson.data
			})
		})
		.catch((error) =>{
			consol.log(error)
		})
	}

	render () {
		return(
			<View style={styles.container}>
				<FlatList
					data= {this.state.dataSource}
					renderItem={this.renderItem}
				></FlatList>
			</View>		
		);
	}
}
