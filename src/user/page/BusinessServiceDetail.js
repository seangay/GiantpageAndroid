import React, {Component} from 'react';
import { StyleSheet, View, ImageBackground, Text } from 'react-native';
import { Constants } from 'expo';
import Lightbox from 'react-native-lightbox';

export default class BusinessServiceDetail extends Component {
    render() {
        return (     
        	<View style={styles.container}>
            
                <ImageBackground
        	        source={require('./../image/businessDetail/2.jpg')}
        		    style={styles.img}
        	    >
        	       <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        Restaurant & Bar
                    </Text>
        	    </ImageBackground>

                <ImageBackground
                    source={require('./../image/businessDetail/3.jpg')}
                    style={styles.img}
                >
                   <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        Room Booking
                    </Text>
                </ImageBackground>

                <ImageBackground
                    source={require('./../image/businessDetail/4.jpg')}
                    style={styles.img}
                >
                   <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        Corporate Events
                    </Text>
                </ImageBackground>

                <ImageBackground
                    source={require('./../image/businessDetail/5.jpg')}
                    style={styles.img}
                >
                   <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        Wedding Hall
                    </Text>
                </ImageBackground>

                <ImageBackground
                    source={require('./../image/businessDetail/6.jpg')}
                    style={styles.img}
                >
                   <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        Travel & Transport
                    </Text>
                </ImageBackground>

                <ImageBackground
                    source={require('./../image/businessDetail/7.jpg')}
                    style={styles.img}
                >
                   <Text
                        size={18}
                        style={styles.paragraph}
                    >
                        All Amenities
                    </Text>
                </ImageBackground>

        	</View>
        );
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#FFFFFF',
    },
    img: {
    	flexGrow:1,
    	width: '100%',
        height: 180,
    	marginTop:5,
    	marginBottom: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,	
    },
    paragraph: {
        margin: 5,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        bottom:  0,
        position: 'absolute',
        color: '#FFFFFF',
        backgroundColor: 'transparent',
    },
});

