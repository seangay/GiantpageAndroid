import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image, View, Text, FlatList, ImageBackground, ScrollView} from 'react-native';
import { Container, Content, Button, Left, Right, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

import Navbar from './../component/Navbar';
import SideMenu from './../component/SideMenu';
import SideMenuDrawer from './../component/SideMenuDrawer';
import Favorite from './Favorite';

export default class Profile extends Component {
    
    constructor() {
        super();
        this.state = {
            user_first_name: 'La',
            user_last_name: 'Yu',
        }
    }

    render() {
        var left = (
            <Left style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='ios-menu-outline'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Left>
        );
        var right = (
            <Right style={{flex:1}}>
                <Button onPress={() => this._sideMenuDrawer.open()} transparent>
                    <Icon
                        name='md-more'
                        type='Ionicons'
                    ></Icon>
                </Button>
            </Right>
        );
        return (
            <SideMenuDrawer ref={(ref) => this._sideMenuDrawer = ref}>
                <Container>
                    <Navbar left={left} right={right} title="Profile" />
                    <Content>
                        <ScrollView>
                            <ImageBackground 
                                style={styles.headerBackgroundImage}
                                blurRadius={10}
                                source={require('./../image/profilebackground.png')}
                            >
                                <Image
                                    style={styles.profileImage}
                                    source={require('./../image/profile.jpg')}
                                    resizeMode="cover"
                                ></Image>
                                /**<View>
                                    <Text style={styles.userNameText}>
                                        {this.state.user_first_name} {this.state.user_last_name}
                                    </Text>
                                </View>
                                **/
                            </ImageBackground>
                            <Favorite />
                        </ScrollView>
                    </Content>
                </Container>
            </SideMenuDrawer>
        );
    }
}

const styles = StyleSheet.create({  
    headerBackgroundImage: {
        paddingBottom: 10,
        paddingTop: 10,
    },
    profileImage: {
        resizeMode: 'cover',
        height: 100,
        width: 100,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'center',
        borderWidth: 1,
        borderRadius: 75,
    },
    userNameText: {
        color: '#FFFFFF',
        fontSize: 22,
        fontWeight: 'bold',
        paddingBottom: 8,
        textAlign: 'center',
    },
});

